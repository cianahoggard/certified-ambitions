from django.urls import path
from business.views import business_list, show_business, create_business, edit_business,homepage



urlpatterns = [
    path("", homepage, name = "homepage"),
    path("business/", business_list, name = "business_list"),
    path("<int:id>/", show_business, name = "show_business"),
    path("create/", create_business, name="create_business"),
    path("<int:id>/edit/", edit_business, name="edit_business"),
]
