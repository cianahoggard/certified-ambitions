from django.forms import ModelForm
from business.models import Business


class BusinessForm(ModelForm):
    class Meta:                 # Step 2
        model = Business          # Step 3
        fields = [              # Step 4
            "title",            # Step 4
            "picture",          # Step 4
            "description",
            "url",     # Step 4
        ]
