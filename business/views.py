from django.shortcuts import render, get_object_or_404
from business.models import Business
from business.forms import BusinessForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


# Create your views here.
def homepage(request):
    business = Business.objects.all()
    context = {
        "business_list": business,
    }
    return render(request, "business/front.html", context)


def show_business(request, id):
    business = get_object_or_404(Business, id=id)
    context = {
        "business_object": business,
    }
    return render(request, "business/detail.html", context)


def business_list(request):
    business = Business.objects.all()
    context = {
        "business_list": business,
    }
    return render(request, "business/list.html", context)


@login_required(login_url='/accounts/login/')
def create_business(request):
    if request.method == "POST":
        form = BusinessForm(request.POST)
        if form.is_valid():
            business = form.save(False)
            business.author = (request.user)
            business.save()
            return redirect("business_list")
    else:
        form = BusinessForm()
    context = {
        "form": form,
    }
    return render(request, "business/create.html", context)


def edit_business(request, id):
    business = get_object_or_404(business, id=id)
    if request.method == "POST":
        form = BusinessForm(request.POST, instance= business)
        if form.is_valid():
            form.save()
            return redirect("show_business", id=id)
    else:
        form = BusinessForm(instance=business)

    context = {
        "business_object": business,
        "Business_form": form,
    }
    return render(request, "business/edit.html", context)

# Create your views here.
