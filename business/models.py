from django.db import models
from django.conf import settings

class Business(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(null=True, blank=True)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    url = models.CharField(max_length=200, null=True, blank=True)


    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="business",
        on_delete=models.CASCADE,
        null=True,
    )


    def __str__(self):
        return self.title


class BusinessStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    business=models.ForeignKey(
        'Business',
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


class Information(models.Model):
    styles = models.CharField(max_length = 100)
    price = models.CharField(max_length = 100)

    business = models.ForeignKey(
        Business,
        related_name="information",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["price"]


# Create your models here.
